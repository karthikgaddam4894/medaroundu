package com.virtus.admin.medaroundu;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

/**
 * Created by AbhiAndroid
 */

public class SplashActivity extends Activity {

    Handler handler;
    ImageView imageView;
    Animation animation_1,animation_2,animation_3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

      /*  handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent(SplashActivity.this,DashBoardActivity.class);
                startActivity(intent);
                finish();
            }
        },3000);*/

          imageView = (ImageView) findViewById(R.id.imvSplash);
          animation_1 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate);
          animation_2 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.antirotate);
          animation_3 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.abc_fade_out);

        startAnimation();
    }

    private void startAnimation() {
        imageView.startAnimation(animation_1);
        animation_1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView.startAnimation(animation_2);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animation_2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
               // imageView.startAnimation(animation_3);
                Intent intent=new Intent(SplashActivity.this,DashBoardActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}